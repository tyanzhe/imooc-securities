package com.imooc.security.core.properties;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class BaseCodeProperties {
	private int length = 6;
	private int expireIn = 60;// 默认60秒失效
	private String url;//需要校验的url
}