package com.imooc.security.core.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "imooc.security")
public class MySecurityProperties {
	private BrowserProperties browser = new BrowserProperties();//登录页面、返回格式
	private ValidateCodeProperties code= new ValidateCodeProperties();
	private SocialProperties social = new SocialProperties();

}

