package com.imooc.security.core.validate.code.image;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

import com.imooc.security.core.validate.code.ValidateCode;

import lombok.Getter;
import lombok.Setter;

public class ImageCode extends ValidateCode{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6020470039852318468L;
	@Getter@Setter
	private BufferedImage image; 
	
	public ImageCode(BufferedImage image, String code, int expireIn){
		super(code, expireIn);
		this.image = image;
	}
	
	public ImageCode(BufferedImage image, String code, LocalDateTime expireTime){
		super(code, expireTime);
		this.image = image;
	}
}