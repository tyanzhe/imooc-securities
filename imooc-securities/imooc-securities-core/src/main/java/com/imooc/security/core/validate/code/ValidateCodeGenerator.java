package com.imooc.security.core.validate.code;

import org.springframework.web.context.request.ServletWebRequest;
/**
 * 校验码接口
 * @author Administrator
 *
 */
public interface ValidateCodeGenerator {

	ValidateCode generate(ServletWebRequest request);
	
}
