package com.imooc.security.core.properties;

import org.springframework.boot.autoconfigure.social.SocialProperties;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class QQProperties extends SocialProperties {
	
	private String providerId = "qq";

}
