
package com.imooc.security.core.properties;

import lombok.Getter;
import lombok.Setter;


@Getter@Setter
public class BrowserProperties {
	
	/**默认表单登录地址*/
	private String loginPage = SecurityConstants.DEFAULT_LOGIN_PAGE_URL;
	/**默认登录类型*/
	private LoginType loginType = LoginType.JSON;
	/**表单登录提交地址*/
	private String loginForm=SecurityConstants.DEFAULT_LOGIN_PROCESSING_URL_FORM;
	private int rememberMeSeconds = 3600;
	private String signUpUrl = "/imooc-signUp.html";
	private SessionProperties session = new SessionProperties();
}
