package com.imooc.security.core.properties;

import lombok.Getter;
import lombok.Setter;

@Getter@Setter
public class ImageCodeProperties extends BaseCodeProperties{
	public ImageCodeProperties() {
		setLength(4);//图形验证码默认为6位
	}
	private int width = 67;
	private int height = 23;
}